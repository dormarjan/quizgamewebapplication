import { createSlice } from '@reduxjs/toolkit';


export const quizGameSlice = createSlice({
    name:"quizGame",
    initialState: [],
    reducers:{

        initGame: (state, action) => {
            return action.payload;
        },

        deleteGame: (state, action) => {
            return state.filter((game) => {
                if (game.id !== action.payload) {
                    return game;
                }
            })
        },
    
    }
});

export const {initGame,deleteGame} = quizGameSlice.actions;

export const selectGame =(state)=> state.quizGame;

export default quizGameSlice.reducer;