import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import validator from "validator";
import usePasswordValidator from "react-use-password-validator";


const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "60%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    margin: "0 auto",
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Reg(props) {
  const classes = useStyles();

  const [username, setUsername] = useState("Example");

  const [email, setEmail] = useState("example@gmail.com");

  const [password, setPassword] = useState("Example1234");

  const [isValid, setIsValid] = usePasswordValidator({
    symbols: false,
  });

  const handleUsername = (event) => {
    setUsername(event.target.value);
  };

  const handleEmail = (event) => {
    setEmail(event.target.value);
  };

  const handlePassword = (event) => {
    setPassword(event.target.value);
    setIsValid(password);
  };

  const handleRegister = async () => {
    const response = await fetch(`http://localhost:4000/quiz/createUser`, {
      method: "POST",
      mode: "cors",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Username: username,
        Email: email,
        Password: password,
      }),
    });
    const data = await response.json();
    if (response.ok) {
      if([data].includes("The")){
        window.alert(data);
      }else{
        window.alert("Registration was successful!");
      }      
    }
  };

  const handleClose = () => props.setShow(false);

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Modal show={props.show} onHide={handleClose} className={classes.paper}>
          <Modal.Header closeButton>
            <Avatar className={classes.avatar}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Sign up
            </Typography>
          </Modal.Header>
          <form className={classes.form} noValidate>
            {username.length >= 4 ? (
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="email"
                autoComplete="username"
                onChange={handleUsername}
                autoFocus
              />
            ) : (
              <TextField
                error
                helperText="Incorrect Username minimum length:4 "
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="username"
                label="Username"
                name="email"
                autoComplete="username"
                onChange={handleUsername}
                autoFocus
              />
            )}

            {validator.isEmail(email) ? (
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={handleEmail}
              />
            ) : (
              <TextField
                error
                helperText="Incorrect email or empty field "
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={handleEmail}
              />
            )}
            {isValid ? (
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePassword}
              />
            ) : (
              <TextField
                error
                helperText="Incorrect password minimum length:6, don't use spaces"
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={handlePassword}
              />
            )}

            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {isValid &&
              validator.isEmail(email) &&
              username.length >= 4 &&
              username !== "example" &&
              email !== "example@example" &&
              password !== "Example1234" ? (
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  onClick={() => {
                    handleClose();
                    handleRegister();
                  }}
                >
                  Sign up
                </Button>
              ) : (
                <Button disabled fullWidth color="primary" variant="contained">
                  Sign up
                </Button>
              )}
            </Modal.Footer>
          </form>
        </Modal>
      </Container>
    </div>
  );
}
