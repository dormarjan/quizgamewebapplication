import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import SportsEsportsIcon from "@material-ui/icons/SportsEsports";
import AddIcon from "@material-ui/icons/Add";
import DnsIcon from "@material-ui/icons/Dns";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { Link, BrowserRouter, Switch, Route } from "react-router-dom";
import Account from "./Account";
import CategorySelector from "./CategorySelector";
import Welcome from "./Welcome";
import MyQuizCategory from "./MyQuizCategory";
import QuizGameSelect from "./QuizGameSelect";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: "#589da6",
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function SideBar(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Have a nice day{" "}
            {props.data.map((user) => {
              return user.Username;
            })}
            !
          </Typography>
        </Toolbar>
      </AppBar>
      <BrowserRouter>
        <Drawer
          variant="permanent"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            }),
          }}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "rtl" ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          <List>
            <Link
              style={{
                textDecoration: "none",
                color: "black",
              }}
              to="/account"
            >
              <ListItem button key="Account">
                <ListItemIcon>
                  <AccountCircleIcon />
                </ListItemIcon>
                <ListItemText primary="Account" />
              </ListItem>
            </Link>
            <Link
              style={{
                textDecoration: "none",
                color: "black",
              }}
              to="/quizmanager"
            >
              <ListItem button key="Quiz Manager">
                <ListItemIcon>
                  <AddIcon />
                </ListItemIcon>
                <ListItemText primary="Quiz Manager" />
              </ListItem>
            </Link>
            <Link
              style={{
                textDecoration: "none",
                color: "black",
              }}
              to="/quizgame"
            >
              <ListItem button key="Quiz Game">
                <ListItemIcon>
                  <SportsEsportsIcon />
                </ListItemIcon>
                <ListItemText primary="Quiz Game" />
              </ListItem>
            </Link>
          </List>
          <Divider />
          <List>
            <Link
              style={{
                textDecoration: "none",
                color: "black",
              }}
              to="/myquiz"
            >
              <ListItem button key="My quiz">
                <ListItemIcon>
                  <DnsIcon />
                </ListItemIcon>
                <ListItemText primary="My quiz" />
              </ListItem>
            </Link>
            <ListItem
              button
              key="Quiz Manager"
              onClick={() => {
                localStorage.clear();
                window.location.reload();
              }}
            >
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary="Exit" />
            </ListItem>
          </List>
        </Drawer>
      
      <Switch>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Route exact path="/">
            <Welcome/>
          </Route>
          <Route exact path="/account">
            <Account data={props.data} />
          </Route>
          <Route exact path="/quizmanager">
            <CategorySelector data={props.data}/>
          </Route>
          <Route exact path="/quizgame">
            <QuizGameSelect data={props.data}/>
          </Route>
          <Route exact path="/myquiz">
          <MyQuizCategory data={props.data} />
          </Route>
        </main>
      </Switch>
      </BrowserRouter>
    </div>
  );
}
