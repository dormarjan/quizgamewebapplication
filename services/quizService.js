
//Backend Services
/*Itt találhatóak az importálások */
//Adatbázis kapcsolat
const getConnection = require("./dbConnection");
//User model (ez egy osztály ami az adatbázisban található Quizuser táblát modelezi le)
const User = require("../models/User");
//Quiz model (ez egy osztály ami az adatbázisban található Quiz táblákat modelezi le)
const Quiz = require("../models/Quiz");
//Score model (ez egy osztály ami az adatbázisban található Score táblát modelezi le)
const Score = require("../models/Score");
//Titkosítási
const bcrypt = require("bcryptjs");

const quizService = {
  //Új User létrehozása
  save: async (user) => {
    try {
      const dbConn = await getConnection();
      let hash = bcrypt.hashSync(user.Password, 10);
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        `INSERT INTO QuizUser (Username,Email,Password) VALUES(?,?,?)`,
        [user.Username, user.Email, hash]
      );
      const newUser = new User(rows.insertId);
      return newUser;
    } catch (error) {
      console.log(error);
    }
  },
//Új Usernek létrehozunk egy Score mezőt 
  createUserScore: async (score) => {
    try {
      const dbConn = await getConnection();
      const userId = score.UserID;
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        `INSERT INTO Score (ScoreOther,ScoreGeography,ScoreHistory,ScoreLiterature,UserID) VALUES(?,?,?,?,?)`,
        [
          score.ScoreOther,
          score.ScoreGeography,
          score.ScoreHistory,
          score.ScoreLiterature,
          userId,
        ]
      );
    } catch (error) {
      console.log(error);
    }
  },
//Vissza adjuk a User pontjait az userID alapján 
  getScore: async (id) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        "SELECT ScoreOther,ScoreGeography,ScoreHistory,ScoreLiterature FROM Score where UserID=?",
        [id]
      );
      const userScore = new Score(
        rows[0].id,
        rows[0].ScoreOther,
        rows[0].ScoreGeography,
        rows[0].ScoreHistory,
        rows[0].ScoreLiterature,
        rows[0].userID
      );
      return userScore;
    } catch (error) {
      console.log(error);
    }
  },
//Regisztrációnál található ellenörzés ami meggátolja , hogy ugyan olyan nével és email címel létrelehesen hozni felhasználát. 
  checkUser: async (user) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        files,
      ] = await dbConn.execute(
        `SELECT*FROM QuizUser WHERE Username=? AND Email =?`,
        [user.Username, user.Email]
      );
      console.log(user.Email, user.Username);
      if (rows.length ==1) {
        return true;
      } else if (rows.length ==0) {
        return false;
      }
    } catch (error) {
      console.log(error);
    }
  },

//Bejelentekezés ha sikeres vissza ajd a a User adatait.
  getUser: async (user) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        `SELECT id,Username,Email,Password FROM QuizUser WHERE Username=? AND Email=? `,
        [user.Username, user.Email]
      );
      if (rows.length === 1) {
        console.log(user.Password);
        console.log(rows[0].Password);
        const check = bcrypt.compareSync(user.Password, rows[0].Password);
        if (check) {
          const data = new User(
            rows[0].id,
            rows[0].Username,
            rows[0].Email,
            ".....",
          );
          console.log(check);
          return data;
        } else if (check === false) {
          console.log(check);
          return 0;
        }
      } else {
        return 0;
      }
    } catch (error) {
      console.log(error);
    }
  },
//Other Quiz táblába történi mentés 
  saveOther: async (quiz) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        "INSERT INTO Other (Question,Answer1,Answer2,Answer3,Answer4,Correct,UserID,Valid) VALUES(?,?,?,?,?,?,?,?)",
        [
          quiz.Question,
          quiz.Answer1,
          quiz.Answer2,
          quiz.Answer3,
          quiz.Answer4,
          quiz.Correct,
          quiz.UserID,
          quiz.Valid,
        ]
      );
      const newQuiz = new Quiz(
        rows.insertId,
        quiz.Question,
        quiz.Answer1,
        quiz.Answer2,
        quiz.Answer3,
        quiz.Answer4,
        quiz.Correct,
        quiz.UserID,
        quiz.Valid
      );
      return newQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Geography quiz táblába történi mentés
  saveGeography: async (quiz) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        "INSERT INTO Geography (Question,Answer1,Answer2,Answer3,Answer4,Correct,UserID,Valid) VALUES(?,?,?,?,?,?,?,?)",
        [
          quiz.Question,
          quiz.Answer1,
          quiz.Answer2,
          quiz.Answer3,
          quiz.Answer4,
          quiz.Correct,
          quiz.UserID,
          quiz.Valid,
        ]
      );
      const newQuiz = new Quiz(
        rows.insertId,
        quiz.Question,
        quiz.Answer1,
        quiz.Answer2,
        quiz.Answer3,
        quiz.Answer4,
        quiz.Correct,
        quiz.UserID,
        quiz.Valid
      );
      return newQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//History Quiz táblába történi mentés
  saveHistory: async (quiz) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        "INSERT INTO History (Question,Answer1,Answer2,Answer3,Answer4,Correct,UserID,Valid) VALUES(?,?,?,?,?,?,?,?)",
        [
          quiz.Question,
          quiz.Answer1,
          quiz.Answer2,
          quiz.Answer3,
          quiz.Answer4,
          quiz.Correct,
          quiz.UserID,
          quiz.Valid,
        ]
      );
      const newQuiz = new Quiz(
        rows.insertId,
        quiz.Question,
        quiz.Answer1,
        quiz.Answer2,
        quiz.Answer3,
        quiz.Answer4,
        quiz.Correct,
        quiz.UserID,
        quiz.Valid
      );
      return newQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Literature Quiz táblába történi mentés
  saveLiterature: async (quiz) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute(
        "INSERT INTO Literature (Question,Answer1,Answer2,Answer3,Answer4,Correct,UserID,Valid) VALUES(?,?,?,?,?,?,?,?)",
        [
          quiz.Question,
          quiz.Answer1,
          quiz.Answer2,
          quiz.Answer3,
          quiz.Answer4,
          quiz.Correct,
          quiz.UserID,
          quiz.Valid,
        ]
      );
      const newQuiz = new Quiz(
        rows.insertId,
        quiz.Question,
        quiz.Answer1,
        quiz.Answer2,
        quiz.Answer3,
        quiz.Answer4,
        quiz.Correct,
        quiz.UserID,
        quiz.Valid
      );
      return newQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//User által létrehozok quizzek vissza adása kategoriánként.
  getOtherQuizByUserID: async (id) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute("SELECT * FROM Other WHERE userID=?", [id]);
      const quizzes = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID,
            row.Valid
          )
      );
      return quizzes;
    } catch (error) {
      console.log(error);
    }
  },
//User által létrehozok quizzek vissza adása kategoriánként.
  getGeoQuizByUserID: async (id) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute("SELECT * FROM Geography WHERE userID=?", [id]);
      const quizzes = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID,
            row.Valid
          )
      );
      return quizzes;
    } catch (error) {
      console.log(error);
    }
  },
//User által létrehozok quizzek vissza adása kategoriánként.
  getHiQuizByUserID: async (id) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute("SELECT * FROM History WHERE userID=?", [id]);
      const quizzes = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID,
            row.Valid
          )
      );
      return quizzes;
    } catch (error) {
      console.log(error);
    }
  },
//User által létrehozok quizzek vissza adása kategoriánként.
  getLiQuizByUserID: async (id) => {
    try {
      const dbConn = await getConnection();
      const [
        rows,
        fields,
      ] = await dbConn.execute("SELECT * FROM Literature WHERE userID=?", [id]);
      const quizzes = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID,
            row.Valid
          )
      );
      return quizzes;
    } catch (error) {
      console.log(error);
    }
  },
//Játékhoz random vissza adunk 5 darabb quizt kategoriánként.
  getGameOther: async () => {
    try {
      const dbConn = await getConnection();
      const [rows, fields] = await dbConn.execute(
        "SELECT* FROM Other Where Valid=1 ORDER BY RAND() LIMIT 5 "
      );
      const gameQuiz = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID
          )
      );
      return gameQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Játékhoz random vissza adunk 5 darabb quizt kategoriánként.
  getGameLi: async () => {
    try {
      const dbConn = await getConnection();
      const [rows, fields] = await dbConn.execute(
        "SELECT* FROM Literature Where Valid=1 ORDER BY RAND() LIMIT 5"
      );
      const gameQuiz = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID
          )
      );
      return gameQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Játékhoz random vissza adunk 5 darabb quizt kategoriánként.
  getGameHi: async () => {
    try {
      const dbConn = await getConnection();
      const [rows, fields] = await dbConn.execute(
        "SELECT* FROM History Where Valid=1 ORDER BY RAND() LIMIT 5"
      );
      const gameQuiz = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID
          )
      );
      return gameQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Játékhoz random vissza adunk 5 darabb quizt kategoriánként.
  getGameGeo: async () => {
    try {
      const dbConn = await getConnection();
      const [rows, fields] = await dbConn.execute(
        "SELECT* FROM Geography Where Valid=1 ORDER BY RAND() LIMIT 5"
      );
      const gameQuiz = rows.map(
        (row) =>
          new Quiz(
            row.id,
            row.Question,
            row.Answer1,
            row.Answer2,
            row.Answer3,
            row.Answer4,
            row.Correct,
            row.UserID
          )
      );
      return gameQuiz;
    } catch (error) {
      console.log(error);
    }
  },
//Quiz törlése
  deleteOther: async (id) => {
    try {
      const dbConn = await getConnection();
      const deleteQuiz = await dbConn.execute("DELETE FROM Other WHERE id =?", [
        id,
      ]);
    } catch (error) {
      console.log(error);
    }
  },
//Quiz törlése
  deleteLi: async (id) => {
    try {
      const dbConn = await getConnection();
      const deleteQuiz = await dbConn.execute(
        "DELETE FROM Literature WHERE id =?",
        [id]
      );
    } catch (error) {
      console.log(error);
    }
  },
//Quiz törlése
  deleteHi: async (id) => {
    try {
      const dbConn = await getConnection();
      const deleteQuiz = await dbConn.execute(
        "DELETE FROM History WHERE id =?",
        [id]
      );
    } catch (error) {
      console.log(error);
    }
  },
//Quiz törlése
  deleteGeo: async (id) => {
    try {
      const dbConn = await getConnection();
      const deleteQuiz = await dbConn.execute(
        "DELETE FROM Geography WHERE id =?",
        [id]
      );
    } catch (error) {
      console.log(error);
    }
  },
//User fiok törlése
  deleteUser: async (id) => {
    try {
      const dbConn = await getConnection();
      const deleScore = await dbConn.execute(
        "DELETE FROM Score WHERE UserID =?",
        [id]
      );
      const deleteOther = await dbConn.execute(
        "DELETE FROM Other WHERE UserID =?",
        [id]
      );
      const deleteHistory = await dbConn.execute(
        "DELETE FROM History WHERE UserID =?",
        [id]
      );
      const deleteGeography = await dbConn.execute(
        "DELETE FROM Geography WHERE UserID =?",
        [id]
      );
      const deleteLiterature = await dbConn.execute(
        "DELETE FROM Literature WHERE UserID =?",
        [id]
      );
      const deleteUser = await dbConn.execute(
        "DELETE FROM QuizeUSer WHERE id =?",
        [id]
      );
    } catch (error) {
      console.log(error);
    }
  },
//Pontszámok frissitése
  updateScore: async (id, score) => {
    try {
      const dbConn = await getConnection();
      let sql = "UPDATE Score SET ";

      for (let key in score) {
        sql += `${key} = ?, `;
      }
      sql = sql.substring(0, sql.length - 2);

      sql += ` WHERE UserID = ${id}`;

      const [rows, fields] = await dbConn.execute(sql, Object.values(score));

      if (rows.affectedRows === 1) {
        console.log(rows.affectedRows);
        return true;
      } else {
        console.log(rows.affectedRows);
        return false;
      }
    } catch (error) {
      console.log(error);
    }
  },
};

module.exports = quizService;
