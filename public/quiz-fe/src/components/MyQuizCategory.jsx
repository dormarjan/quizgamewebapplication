import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import OtherQuiz from "./OtherQuiz";
import GeoQuiz from "./GeoQuiz";
import LiQuiz from "./LiQuiz";
import HiQuiz from "./HiQuiz";
import PublicIcon from "@material-ui/icons/Public";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import BookIcon from "@material-ui/icons/Book";
import DynamicFeedIcon from "@material-ui/icons/DynamicFeed";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    width: 600,
    height: 65,
    margin: "0 auto",
    backgroundColor: "#F2F2F2",
    boxShadow: "2px 2px 5px grey",
    maxwidth: "1000px",
    borderRadius: "10px",
    marginBottom: "40px",
  },
});

export default function MyQuizCategory(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState("");

  const handleChange = (event, newValue) => {
    setValue(newValue);
    console.log(newValue);
  };

  return (
    <Router>
      <Typography gutterBottom variant="h5" component="h2">
        Choose category !
      </Typography>

      <BottomNavigation
        value={value}
        onChange={handleChange}
        className={classes.root}
        showLabels
      >
        <Link
          to="hi"
          style={{
            textDecoration: "none",
          }}
        >
          <BottomNavigationAction
            label="History"
            value="Hisrory"
            onClick={handleChange}
            icon={<AccountBalanceIcon fontSize="large" />}
          />
        </Link>
        <Link
          to="geo"
          style={{
            textDecoration: "none",
          }}
        >
          <BottomNavigationAction
            label="Geography"
            value="Geography"
            icon={<PublicIcon fontSize="large" />}
          />
        </Link>
        <Link
          to="li"
          style={{
            textDecoration: "none",
          }}
        >
          <BottomNavigationAction
            label="Literature"
            value="Literature"
            icon={<BookIcon fontSize="large" />}
          />
        </Link>

        <Link
          to="other"
          style={{
            textDecoration: "none",
          }}
        >
          <BottomNavigationAction
            label="Other"
            value="Other"
            icon={<DynamicFeedIcon fontSize="large" />}
          />
        </Link>
      </BottomNavigation>
      <Switch>
        <Route path="/li">
          {" "}
          <LiQuiz data={props.data} />{" "}
        </Route>
        <Route path="/geo">
          {" "}
          <GeoQuiz data={props.data} />{" "}
        </Route>
        <Route path="/hi">
          {" "}
          <HiQuiz data={props.data} />{" "}
        </Route>
        <Route path="/other">
          <OtherQuiz data={props.data} />
        </Route>
      </Switch>
    </Router>
  );
}
