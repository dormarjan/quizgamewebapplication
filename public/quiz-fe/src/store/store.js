import { configureStore } from '@reduxjs/toolkit';
import loginReducer from "./loginSlice";
import userQuizReducer from "./userQuizSlice";
import quizGameReducer from "./quizGameSlice";
import quizStartReducer from "./quizStartSlice";
import gameIdReducer from "./gameIdSlice";
import scoreReducer from "./scoreSlice";
//Redux store
export default configureStore({
    reducer:{
        userData:loginReducer,
        userQuiz:userQuizReducer,
        quizGame:quizGameReducer,
        quizStart:quizStartReducer,
        gameId:gameIdReducer,
        score:scoreReducer,

    }
})