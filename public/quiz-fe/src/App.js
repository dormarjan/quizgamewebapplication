import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Login from "./components/Login";
import SideBar from "./components/SideBar";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { initUser} from "./store/loginSlice";


function App() {
  const dispatch = useDispatch();

  const loadFromLocalStorage = () => {
    const data = localStorage.getItem("user");
    if (data == null || data.length === 0) {
      return null;
    } else {
      dispatch(initUser(data));
      return JSON.parse(data);
    }
  };

  const data= loadFromLocalStorage();


  return (
    <div className="App">
      {data !== null ? (
        <div>
          <SideBar data={[data]} />
        </div>
      ) : (
        <Login />
      )}
    </div>
  );
}

export default App;
