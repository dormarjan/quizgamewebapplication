import { createSlice } from "@reduxjs/toolkit";

export const gameIdSlice = createSlice({
  name: "gameId",
  initialState: [],
  reducers: {
    initId: (state, action) => {
      return action.payload;
    },

    deleteId: (state, action) => {
      return state.filter((gameid) => {
        if (gameid.id !== action.payload) {
          return gameid;
        }
      });
    },
  },
});

export const { initId, deleteId } = gameIdSlice.actions;

export const selectGameid = (state) => state.gameId;

export default gameIdSlice.reducer;
