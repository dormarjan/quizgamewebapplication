import { createSlice } from '@reduxjs/toolkit';

export const userQuizSlice = createSlice({
    name:"userQuiz",
    initialState:[],
    reducers:{
        
        initQuiz:(state, action)=>{
            return action.payload;
        },

        deleteQuiz: (state, action) => {
            return state.filter((quiz) => {
                if (quiz.id !== action.payload) {
                    return quiz;
                }
            })
        },
    }
});

export const {initQuiz,deleteQuiz} = userQuizSlice.actions;

export const selectUserQuiz = (state)=> state.userQuiz;

export default userQuizSlice.reducer;