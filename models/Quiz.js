//Quiz model
class Quiz {
  constructor(id,Question,Answer1,Answer2,Answer3,Answer4,Correct,UserID,Valid) {
    this.id = id;
    this.Question = Question;
    this.Answer1 = Answer1;
    this.Answer2 = Answer2;
    this.Answer3 = Answer3;
    this.Answer4 = Answer4;
    this.Correct = Correct;
    this.UserID = UserID;
    this.Valid = Valid;
  }

  getId() {
    return this.id;
  }

  getQuestion() {
    return this.Question;
  }

  getAnswer1() {
    return this.Answer1;
  }

  getAnswer2() {
    return this.Answer2;
  }

  getAnswer3() {
    return this.Answer3;
  }

  getAnswer4() {
    return this.Answer4;
  }

  getCorrect() {
    return this.Correct;
  }

  getUserID() {
    return this.UserID;
  }
  getValid() {
    return this.Valid;
  }
}
module.exports = Quiz;
