import { createSlice } from "@reduxjs/toolkit";

export const scoreSlice = createSlice({
  name: "score",
  initialState: [],
  reducers: {
    initScore: (state, action) => {
      return action.payload;
    },

    deleteScore: (state, action) => {
      return state.filter((scr) => {
        if (scr.id !== action.payload) {
          return scr;
        }
      });
    },
  },
});

export const { initScore, deleteScore } = scoreSlice.actions;

export const selectScore = (state) => state.score;

export default scoreSlice.reducer;