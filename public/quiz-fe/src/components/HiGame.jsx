import { useDispatch, useSelector } from "react-redux";
import { deleteGame, selectGame } from "../store/quizGameSlice";
import { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { selectScore } from "../store/scoreSlice";
import { Slide } from "react-awesome-reveal";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import "bootstrap/dist/css/bootstrap.min.css";
import quizImage from "../pngs/quizimage.png";
import { CircularProgressbar } from "react-circular-progressbar";

const useStyles = makeStyles({
  root: {
    width: 1000,
    height: 425,
    margin: "0 auto",
    padding: 60,
    backgroundColor: "#F2F2F2",
    boxShadow: "2px 2px 5px grey",
    maxwidth: "1000px",
    borderRadius: "10px",
    marginBottom: "40px",
  },

  button: {
    margin: "0 auto",
    width: "300px",
    height: "50px",
    marginTop: "10px",
  },
});

export default function OtherGame(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const game = useSelector(selectGame);
  const previousScore = useSelector(selectScore);

  let [score, setScore] = useState(0);
  const [buttonColor1, setButtonColor1] = useState("secondary");
  const [buttonColor2, setButtonColor2] = useState("secondary");
  const [buttonColor3, setButtonColor3] = useState("secondary");
  const [buttonColor4, setButtonColor4] = useState("secondary");
  const [triger, setTriger] = useState("hidden");

  let id = props.data.map((user) => user.id);
  let preScore = previousScore.map((score) => {
    return score.ScoreHistory;
  });
  let newScore = parseInt(preScore) + score;

  const handleChange = () => {
    props.handleScoreUpdate({ id: id[0], ScoreHistory: newScore });
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  };

  const handleGameid = (id) => {
    setTimeout(() => {
      dispatch(deleteGame(id));
    }, 2000);

    console.log(score);
  };

  return (
    <Slide left>
      <div>
        <div
          style={{
            maxWidth: "50%",
            margin: "0 auto",
          }}
        >
          <img
            style={{
              maxWidth: "50%",
              maxHeight: "10%",
              marginBottom: 30,
            }}
            src={quizImage}
            alt="li"
          />
        </div>
        {game.length === 0 ? (
          <div>
            <div
              style={{
                width: 200,
                height: 200,
                pathTransitionDuration: 0.5,
                margin: "0 auto",
                marginBottom:"10px"
              }}
            >
              <CircularProgressbar
                value={score}
                text={score}
                maxValue={5}
              />
            </div>
            <Button onClick={() => handleChange()}> Get Score !</Button>
          </div>
        ) : (
          game.map((quiz) =>
            quiz === game[0] ? (
              <div className={classes.root}>
                <h1
                  style={{
                    marginBottom: 50
                  }}
                >
                  {quiz.Question}
                </h1>
                <div
                  style={{
                    margin: "0px auto",
                  }}
                >
                  <Container>
                    <Row>
                      <Col>
                        <Button
                          className={classes.button}
                          variant={buttonColor1}
                          value={quiz.Answer1}
                          onClick={(event) => {
                            if (event.target.value === quiz.Correct) {
                              setScore((score += 1));
                              console.log(score);
                              setButtonColor1("success");
                              setTriger("visible")
                              setTimeout(() => {
                                setButtonColor1("secondary");
                                setTriger("hidden")
                              }, 2000);
                            } else {
                              setButtonColor1("danger");
                              setTriger("visible")
                              setTimeout(() => {
                                setButtonColor1("secondary");
                                setTriger("hidden")
                              }, 2000);
                            }
                            handleGameid(quiz.id);
                          }}
                        >
                          {quiz.Answer1}
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          className={classes.button}
                          variant={buttonColor2}
                          value={quiz.Answer2}
                          onClick={(event) => {
                            if (event.target.value === quiz.Correct) {
                              setScore((score += 1));
                              console.log(score);
                              setButtonColor2("success");
                              setTriger("visible")
                              setTimeout(() => {
                                setButtonColor2("secondary");
                                setTriger("hidden")

                              }, 2000);
                            } else {
                              setButtonColor2("danger");
                              setTriger("visible")
                              setTimeout(() => {
                                setButtonColor2("secondary");
                                setTriger("hidden")
                              }, 2000);
                            }
                            handleGameid(quiz.id);
                          }}
                        >
                          {quiz.Answer2}
                        </Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Button
                          className={classes.button}
                          variant={buttonColor3}
                          value={quiz.Answer3}
                          onClick={(event) => {
                            if (event.target.value === quiz.Correct) {
                              setScore((score += 1));
                              console.log(score);
                              setTriger("visible")
                              setButtonColor3("success");
                              setTimeout(() => {
                                setButtonColor3("secondary");
                                setTriger("hidden")
                              }, 2000);
                            } else {
                              setTriger("visible")
                              setButtonColor3("danger");
                              setTimeout(() => {
                                setButtonColor3("secondary");
                                setTriger("hidden")
                              }, 2000);
                            }
                            handleGameid(quiz.id);
                          }}
                        >
                          {quiz.Answer3}
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          className={classes.button}
                          variant={buttonColor4}
                          value={quiz.Answer4}
                          onClick={(event) => {
                            if (event.target.value === quiz.Correct) {
                              setScore((score += 1));
                              console.log(score);
                              setTriger("visible")
                              setButtonColor4("success");
                              setTimeout(() => {
                                setTriger("hidden")
                                setButtonColor4("secondary");
                              }, 2000);
                            } else {
                              setTriger("visible")
                              setButtonColor4("danger");
                              setTimeout(() => {
                                setTriger("hidden")
                                setButtonColor4("secondary");
                              }, 2000);
                            }
                            handleGameid(quiz.id);
                          }}
                        >
                          {quiz.Answer4}
                        </Button>
                      </Col>
                    </Row>
                  </Container>
                  <div>
                    <Button className={classes.button} style={{
                      visibility: `${triger}`
                    }} variant="success">{quiz.Correct}</Button>
                  </div>
                </div>
              </div>
            ) : (
              " "
            )
          )
        )}
      </div>
    </Slide>
  );
}
