//User model
class User {
  constructor(id, Username, Email, Password) {
    this.id = id,
    this.Username = Username,
    this.Email = Email,
    this.Password = Password
  }

  getId() {
    return this.id;
  }

  getUsername() {
    return this.Username;
  }

  getEmail() {
    return this.Email;
  }

  getPassword() {
    let salt = bcrypt.genSaltSync(10);
    return this.Password,salt;
  }

}

module.exports=User;