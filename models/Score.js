//Score model
class Score{
    constructor(id,ScoreOther,ScoreGeography,ScoreHistory,ScoreLiterature,UserID){
        this.id = id;
        this.ScoreOther = ScoreOther;
        this.ScoreGeography= ScoreGeography;
        this.ScoreHistory = ScoreHistory;
        this.ScoreLiterature = ScoreLiterature;
        this.UserID = UserID;

    }

    getId() {
        return this.id;

    }

    getScoreOther(){
        return this.ScoreOther;
    }

    getScoreHistory(){
        return this.ScoreHistory;
    }

    getScoreLiterature(){
        return this.ScoreLiterature;

    }

    getUserID(){
       return this.userID ;
    }
}
module.exports=Score;