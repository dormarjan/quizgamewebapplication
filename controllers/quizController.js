const express = require("express");
const router = express.Router();

const quizService = require("../services/quizService");

//User létrehozás
router.post("/createUser", async (req, res) => {
  try {
    const user = req.body;

    const checkData = await quizService.checkUser(user);
    if (checkData == false) {
      console.log(checkData);
      const createUser = await quizService.save(user);
      res.json(createUser);
      const userID = createUser.id;
      console.log(userID);
      const score = {
        ScoreOther: "0",
        ScoreGeography: "0",
        ScoreHistory: "0",
        ScoreLiterature: "0",
        UserID: `${userID}`,
      };
      const createUserScore = await quizService.createUserScore(score);
    } else if (checkData == true) {
      console.log(checkData);
      res.json("The Username and Email already exist");
    }
  } catch (error) {
    console.log(error);
  }
});
//User login 
router.post("/user", async (req, res) => {
  const user = req.body;
  const getUser = await quizService.getUser(user);
  if (getUser !== 0) {
    res.json(getUser);
    console.log(getUser);
  } else {
    res.json("Incorrect password or email");
  }
});
//User pontszám vissza adás 
router.get("/userScore/:id", async (req, res) => {
  const userScore = await quizService.getScore(req.params.id);
  res.json(userScore);
});

router.post("/createOther", async (req, res) => {
    try{
        const quiz = req.body;
        const saveOther = await quizService.saveOther(quiz);
        res.json(saveOther);

    }catch(error){
        console.log(error);
    }
 
});
//Quiz létrehozás egy adott kategoriában
router.post("/createGeography", async (req, res) => {
  const quiz = req.body;
  const saveGeography = await quizService.saveGeography(quiz);
  res.json(saveGeography);
});
//Quiz létrehozás egy adott kategoriában
router.post("/createHistory", async (req, res) => {
  const quiz = req.body;
  const saveHistory = await quizService.saveHistory(quiz);
  res.json(saveHistory);
});
//Quiz létrehozás egy adott kategoriában
router.post("/createLiterature", async (req, res) => {
  const quiz = req.body;
  const saveLiterature = await quizService.saveLiterature(quiz);
  res.json(saveLiterature);
});
//User áltál feltöltött quiz visszaadása egy adott kategoriában
router.get("/otherquizbyuser/:id", async (req, res) => {
  res.json({
    quizzes: await quizService.getOtherQuizByUserID(parseFloat(req.params.id)),
  });
  console.log({
    quizzes: await quizService.getOtherQuizByUserID(parseFloat(req.params.id)),
  });
});
//User áltál feltöltött quiz visszaadása egy adott kategoriában
router.get("/geoquizbyuser/:id", async (req, res) => {
  res.json({
    quizzes: await quizService.getGeoQuizByUserID(parseFloat(req.params.id)),
  });
  console.log({
    quizzes: await quizService.getGeoQuizByUserID(parseFloat(req.params.id)),
  });
});
//User áltál feltöltött quiz visszaadása egy adott kategoriában
router.get("/hiquizbyuser/:id", async (req, res) => {
  res.json({
    quizzes: await quizService.getHiQuizByUserID(parseFloat(req.params.id)),
  });
  console.log({
    quizzes: await quizService.getHiQuizByUserID(parseFloat(req.params.id)),
  });
});
//User áltál feltöltött quiz visszaadása egy adott kategoriában
router.get("/liquizbyuser/:id", async (req, res) => {
  res.json({
    quizzes: await quizService.getLiQuizByUserID(parseFloat(req.params.id)),
  });
  console.log({
    quizzes: await quizService.getLiQuizByUserID(parseFloat(req.params.id)),
  });
});
//Valid random quizek vissza adása egy kategoriában
router.get("/gameOther", async (req, res) => {
  const gameQuiz = await quizService.getGameOther();
  res.json(gameQuiz);
});
//Valid random quizek vissza adása egy kategoriában
router.get("/gameLi", async (req, res) => {
  const gameQuiz = await quizService.getGameLi();
  res.json(gameQuiz);
});
//Valid random quizek vissza adása egy kategoriában
router.get("/gameHi", async (req, res) => {
  const gameQuiz = await quizService.getGameHi();
  res.json(gameQuiz);
});
//Valid random quizek vissza adása egy kategoriában
router.get("/gameGeo", async (req, res) => {
  const gameQuiz = await quizService.getGameGeo();
  res.json(gameQuiz);
});
//Quiz törlés
router.delete("/deleotherQuiz/:id", async (req, res) => {
  await quizService.deleteOther(parseFloat(req.params.id));
  res.json("Delete was successful!");
});
//Quiz törlés
router.delete("/deleliQuiz/:id", async (req, res) => {
  await quizService.deleteLi(parseFloat(req.params.id));
  res.json("Delete was successful!");
});
//Quiz törlés
router.delete("/delehiQuiz/:id", async (req, res) => {
  await quizService.deleteHi(parseFloat(req.params.id));
  res.json("Delete was successful!");
});
//Quiz törlés
router.delete("/deleteGeo/:id", async (req, res) => {
  await quizService.deleteGeo(parseFloat(req.params.id));
  res.json("Delete was successful!");
});
//User törlés
router.delete("/deleteUser/:id", async (req, res) => {
  await quizService.deleteUser(parseFloat(req.params.id));
  res.json("Delete was successful!");
});
//Score frissités
router.put("/updateScore/:id", async (req, res) => {
  const id = parseFloat(req.params.id);
  const score = req.body;
  const success = await quizService.updateScore(id, score);
  if (success) {
    res.json(await quizService.getScore(id));
  } else {
    res.sendStatus(400);
  }
});

module.exports = router;
