import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useState,useEffect } from "react";
import { useDispatch  } from "react-redux";
import { initScore } from "../store/scoreSlice";
import { initGame } from "../store/quizGameSlice";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import IconButton from "@material-ui/core/IconButton";
import Higame from "./HiGame";
import LiGame from "./LiGame";
import GeoGame from "./GeoGame";
import OtherGame from "./OtherGame";
import quizImage from "../pngs/quizimage.png";
import { Slide } from "react-awesome-reveal";


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 300,
    margin: "1rem",
    display: "flex",
    minHeight: 400,
  },
  media: {
    height: 140,
  },
});

export default function QuizGameSelect(props) {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [literature, setLiterature] = useState(false);

  const [history, setHistory] = useState(false);

  const [geography, setGeography] = useState(false);

  const [other, setOther] = useState(false);

  const handleScore = async (id) => {
    const response = await fetch(`http://localhost:4000/quiz/userScore/${id}`, {
      method: "GET",
    });
    const data = await response.json();
    if (response.ok) {
      dispatch(initScore([data]));
    }
  };

  const handleLi = async()=>{
      const response = await fetch("http://localhost:4000/quiz/gameLi",{
          method: "GET"
      });
      const data = await response.json();
      dispatch(initGame(data));
  }

  const handleOther = async()=>{
    const response = await fetch("http://localhost:4000/quiz/gameOther",{
        method: "GET"
    });
    const data = await response.json();
    dispatch(initGame(data));
}

const handleHi = async()=>{
    const response = await fetch("http://localhost:4000/quiz/gameHi",{
        method: "GET"
    });
    const data = await response.json();
    dispatch(initGame(data));
}

const handleGeo = async()=>{
    const response = await fetch("http://localhost:4000/quiz//gameGeo",{
        method: "GET"
    });
    const data = await response.json();
    dispatch(initGame(data));
}
let id = props.data.map((user) => {
  return user.id;
});

const handleScoreUpdate = async (data) => {

  console.log(id)
  await fetch(`http://localhost:4000/quiz/updateScore/${data.id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data)
  });
};

useEffect(() =>{
  handleScore(id)
},[])

  return (
    <div>
      {history ? (
        <Higame data={props.data} handleScoreUpdate={handleScoreUpdate} />
      ) : literature ? (
        <LiGame data={props.data} handleScoreUpdate={handleScoreUpdate} />
      ) : geography ? (
        <GeoGame data={props.data} handleScoreUpdate={handleScoreUpdate} />
      ) : other ? (
        <OtherGame data={props.data} handleScoreUpdate={handleScoreUpdate} />
      ) : (
        <Slide left>
          <div
            style={{
              maxWidth: "70%",
              margin: "0 auto",
            }}
          >
            <img
              style={{
                maxWidth: "70%",
                maxHeight: "10%",
                marginBottom: 30,
              }}
              src={quizImage}
              alt="li"
            />
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              flexWrap: "wrap",
            }}
          >
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  style={{
                    marginTop: "15px",
                  }}
                  component="img"
                  alt="Contemplative Reptile"
                  height="170"
                  image="/imgs/hi.jpg"
                  title="Hi"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    History
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    This is the history category, here you will come across historical themed quiz questions.
                  </Typography>
                </CardContent>

                <IconButton onClick={(()=>{
                    setHistory(true)
                    handleHi();
                })}>
                  <AddCircleIcon fontSize="large" />
                </IconButton>
              </CardActionArea>
            </Card>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Contemplative Reptile"
                  height="170"
                  image="/imgs/geo.jpg"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Geography
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    This is the geography category, here you will come across geography themed quiz questions.
                  </Typography>
                </CardContent>

                <IconButton onClick={(()=>{
                    setGeography(true);
                    handleGeo();
                })}>
                  <AddCircleIcon fontSize="large" />
                </IconButton>
              </CardActionArea>
            </Card>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Contemplative Reptile"
                  height="170"
                  image="/imgs/li.jpg"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Literature
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    This is the literature category, here you will encounter literary themed quiz questions.
                  </Typography>
                </CardContent>

                <IconButton onClick={(()=>{
                    setLiterature(true);
                    handleLi();
                })}>
                  <AddCircleIcon fontSize="large" />
                </IconButton>
              </CardActionArea>
            </Card>
            <Card className={classes.root}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  alt="Contemplative Reptile"
                  height="170"
                  image="/imgs/other.jpg"
                  title="Contemplative Reptile"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Other
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    This is the other category, here you will come across quiz questions on all sorts of topics that are not covered in the previous three topics.
                  </Typography>
                </CardContent>

                <IconButton onClick={(()=>{
                    setOther(true);
                    handleOther();
                })}>
                  <AddCircleIcon fontSize="large" />
                </IconButton>
              </CardActionArea>
            </Card>
          </div>
        </Slide>
      )}
    </div>
  );
}
