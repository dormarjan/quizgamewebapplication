import welcome from "../pngs/welcome.png";
import { Slide } from "react-awesome-reveal";

export default function Welcome() {
  return (
    <div>
      <Slide left>
        <img
          style={{
            maxWidth: "70%",
          }}
          src={welcome}
          alt="li"
        />
      </Slide>
    </div>
  );
}
