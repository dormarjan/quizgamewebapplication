import { createSlice } from "@reduxjs/toolkit";

export const quizStartSlice = createSlice({
  name: "quizStart",
  initialState: [],
  reducers: {
    initStart: (state, action) => {
      return action.payload;
    },

    deleteStart: (state, action) => {
      return state.filter((start) => {
        if (start.id !== action.payload) {
          return start;
        }
      });
    },
  },
});

export const { initStart, deleteStart } = quizStartSlice.actions;

export const selectStart = (state) => state.quizStart;

export default quizStartSlice.reducer;
