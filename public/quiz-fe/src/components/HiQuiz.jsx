import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { initQuiz, selectUserQuiz,deleteQuiz } from "../store/userQuizSlice";
import { useDispatch,useSelector  } from "react-redux";
import { useEffect } from "react";
import { Fade } from "react-awesome-reveal";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    minWidth: 300,
    margin: "1rem",
  },
  media: {
    height: 140,
  },
});

export default function HiQuiz(props) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const quizStore = useSelector(selectUserQuiz);

  let id;

  const handleDelete = async (id) => {
    const response = await fetch(`http://localhost:4000/quiz/delehiQuiz/${id}`, {
      method: "DELETE",
    });
    const data = await response.json();
    dispatch(deleteQuiz(id))
    window.alert(data);
  };

  const getQuiz = async (id) => {
    props.data.map((user) => {
      return (id = user.id);
    });

    const response = await fetch(
      `http://localhost:4000/quiz/hiquizbyuser/${id}`,
      {
        method: "GET",
        mode: "cors",
      }
    );
    const data = await response.json();
    console.log(data);
    dispatch(initQuiz(data.quizzes));
  };

  useEffect(() => {
    setTimeout(() => {
      getQuiz(id);
    }, 500);
  }, []);

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-around",
        flexWrap: "wrap",
      }}
    >
      {quizStore.length !== 0
        ? quizStore.map((Quiz) => {
            return (
              <Fade left>
                <Card className={classes.root}>
                  <CardActionArea>
                    <CardMedia
                      className={classes.media}
                      image="https://i.pinimg.com/originals/09/b0/08/09b008ceb45878eb34180d23506e4212.gif"
                      title="Question mark"
                    />
                    <CardContent>
                      <h6>Question</h6>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="h2"
                        style={{
                          fontStyle: "italic",
                          fontWeight: "bold",
                        }}
                      >
                        {Quiz.Question}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        color="secondary"
                        component="p"
                      >
                        {Quiz.Answer1}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        color="secondary"
                        component="p"
                      >
                        {Quiz.Answer2}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        color="secondary"
                        component="p"
                      >
                        {Quiz.Answer3}
                      </Typography>
                      <Typography
                        variant="subtitle1"
                        color="secondary"
                        component="p"
                      >
                        {Quiz.Answer4}
                      </Typography>
                      <h6>Correct answer</h6>
                      <Typography variant="body2" color="primary" component="p">
                        {Quiz.Correct}
                      </Typography>
                      <Typography variant="body2" color="primary" component="p">
                        {Quiz.Valid===0?"Awaiting approval":"Approved"}
                      </Typography>
                    </CardContent>
                    <IconButton
                      aria-label="delete"
                      className={classes.button}
                      onClick={(()=>{
                        handleDelete(Quiz.id)
                      })}
                    >
                      <DeleteIcon fontSize="large" />
                    </IconButton>
                  </CardActionArea>
                </Card>
              </Fade>
            );
          })
        : <div>
          <h2>You haven't created a quiz in this category yet</h2>
          <img  src="https://i.pinimg.com/originals/09/b0/08/09b008ceb45878eb34180d23506e4212.gif" fluid alt=""/></div>
          }
    </div>
  );
}
