const express = require('express');
const cors = require('cors');
const app = express();
const port = 4000;

const quizController = require("./controllers/quizController");

app.use(cors());

app.use(express.json());

app.use('/', express.static('public'));

app.use('/quiz', quizController);

app.listen(port,()=>{
    console.log(`Example app listening at http://localhost:${port}`)
});